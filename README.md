# Liferay Guestbook

DEVELOPING A WEB APPLICATION
  <ul>
   <li>Using Resources and Permissions</li>
   <li>Search by Indexer</li>
   <li>Assets: Integrating with Liferay’s Framework
    <ul>
     <li>Creating JSPs for Displaying Custom Assets in the Asset Publisher</li>
     <li>Enabling Tags, Categories, and Related Assets</li>
     <li> Enabling Comments and Ratings for Guestbook Entries</li>
    </ul>
   </li>
   <li>Generating Web Services</li>
   <li>Using Workflow</li>
   <li>Enabling Staging and Export/Import</li>
  </ul>