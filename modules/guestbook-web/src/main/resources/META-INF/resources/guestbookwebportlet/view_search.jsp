<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>
<%@ page import="com.liferay.portal.kernel.exception.SystemException" %>
<%@ page import="com.liferay.portal.kernel.log.Log" %>
<%@ page import="com.liferay.portal.kernel.log.LogFactoryUtil" %>
<%@ page import="com.liferay.portal.kernel.search.*" %>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@include file="../init.jsp" %>

<liferay-portlet:renderURL varImpl="searchURL">
    <portlet:param name="mvcPath" value="/guestbookwebportlet/view_search.jsp"/>
</liferay-portlet:renderURL>

<portlet:renderURL var="viewURL">
    <portlet:param name="mvcPath" value="/guestbookwebportlet/view.jsp"/>
</portlet:renderURL>

<%
    String keywords = ParamUtil.getString(request, "keywords");
    long guestbookId = ParamUtil.getLong(renderRequest, "guestbookId");
%>

<%!
    private static Log _log = LogFactoryUtil.getLog("html.guestbookwebportlet.view_search_jsp");
%>

<aui:form action="<%= searchURL %>" method="get" name="fm">
    <liferay-portlet:renderURLParams varImpl="searchURL"/>
    <liferay-ui:header backURL="<%= viewURL.toString() %>" title="search"/>
    <div class="search-form">
        <aui:input inlineField="true" label="" name="keywords" size="30" title="search-entries"/>
        <aui:button style="vertical-align: top;" type="submit" value="search"/>
    </div>
</aui:form>

<%
    SearchContext searchContext = SearchContextFactory
            .getInstance(request);
    searchContext.setKeywords(keywords);
    searchContext.setAttribute("paginationType", "more");
    searchContext.setStart(0);
    searchContext.setEnd(10);
    Indexer indexer = IndexerRegistryUtil.getIndexer(Entry.class);
    Hits hits = indexer.search(searchContext);
    List<Entry> entries = new ArrayList<Entry>();
    for (int i = 0; i < hits.getDocs().length; i++) {
        Document doc = hits.doc(i);
        long entryId = GetterUtil
                .getLong(doc.get(Field.ENTRY_CLASS_PK));
        Entry entry = null;
        try {
            entry = EntryLocalServiceUtil.getEntry(entryId);
        } catch (PortalException | SystemException pe) {
            _log.error(pe.getLocalizedMessage());
        }
        entries.add(entry);
    }
    List<Guestbook> guestbooks = GuestbookLocalServiceUtil.getGuestbooks(scopeGroupId);
    Map<String, String> guestbookMap = new HashMap<String, String>();
    for (Guestbook guestbook : guestbooks) {
        guestbookMap.put(Long.toString(guestbook.getGuestbookId()), guestbook.getName());
    }
%>

<liferay-ui:search-container delta="10" emptyResultsMessage="no-entries-were-found" total="<%= entries.size() %>">
    <liferay-ui:search-container-results results="<%= entries %>"/>
    <liferay-ui:search-container-row className="com.liferay.docs.guestbook.model.Entry" keyProperty="entryId" modelVar="entry" escapedModel="<%=true%>">
        <liferay-ui:search-container-column-text name="guestbook" value="<%=guestbookMap.get(Long.toString(entry.getGuestbookId()))%>"/>
        <liferay-ui:search-container-column-text property="message"/>
        <liferay-ui:search-container-column-text property="name"/>
        <liferay-ui:search-container-column-jsp path="/guestbookwebportlet/entry_actions.jsp" align="right"/>
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator/>
</liferay-ui:search-container>