create index IX_64831986 on aim_Entry (groupId, guestbookId, status);
create index IX_7ED4DEB2 on aim_Entry (groupId, status);
create index IX_C3561B84 on aim_Entry (status);
create index IX_F995B112 on aim_Entry (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_EC9B5414 on aim_Entry (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_23045503 on aim_Guestbook (groupId, status);
create index IX_D1861613 on aim_Guestbook (status);
create index IX_DB5504E1 on aim_Guestbook (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_26B4FA3 on aim_Guestbook (uuid_[$COLUMN_LENGTH:75$], groupId);